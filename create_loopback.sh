#!/bin/bash

# create a null sink to drop all audio in
pactl load-module module-null-sink sink_name=mixaudio sink_properties=device.description="mixaudio"

# crete a virtual source that broadcasts all sink output
pactl load-module module-virtual-source source_name=virtmic master=mixaudio.monitor

# loopback main mic to the sink
DEFAULT_SOURCE=$(pactl info | grep "Default Source")
AUDIO_SOURCE=${DEFAULT_SOURCE#*: }
pactl load-module module-loopback adjust_time=0 latency_msec=1 source=$AUDIO_SOURCE sink=mixaudio

# now just use virtmic in your apps to have your microphone and all other audio you drop to mixaudio broadcasted
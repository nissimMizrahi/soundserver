#!/usr/bin/python

import vlc
from weebserver import BasicServer

from config import *


def get_audio_outputs():
    med = vlc.Instance().media_player_new()
    mods = med.audio_output_device_enum()

    if not mods:
        return []

    devices = []
    mod = mods
    while mod:
        mod = mod.contents
        devices.append(mod.device)
        mod = mod.next
    
    return devices


class SoundServer(BasicServer):

    audio_devices = get_audio_outputs()


    def _pick_audio_device(self, dev_name): 
        for dev in self.audio_devices:
            if dev_name in dev.decode().lower():
                return dev

        return None


    def _handle_query_with_devices(self, query, volume):
        for dev_name in query.device:
            track = vlc.MediaPlayer(query.path[0])
            device = self._pick_audio_device(dev_name)
            
            print("playing", query.path[0], "on", device)
            
            track.audio_output_device_set(None, device)
            track.audio_set_volume(volume)
            track.play()


    def handle_get_update(self):
        self.audio_devices = get_audio_outputs()
        return self.send_content(200, b"success")


    def handle_get_play(self):
        query = self._query

        if not hasattr(query, "path"):
            return self.send_content(500, b"must specify path and device")

        volume = 100
        if hasattr(query, "volume"):
            volume = int(query.volume[0])
        
        if not hasattr(query, "device"):
            track.play()
            return self.send_content(200, b"success")

        self._handle_query_with_devices(query, volume)
        return self.send_content(200, b"success")


    def do_GET(self):
        return self.default_route_request("get")



def main():
    server = SoundServer.get_threading_Server(SERVER_ADDR)
    server.serve_forever()



if __name__ == "__main__":
    main()
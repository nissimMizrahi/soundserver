from http.server import *
from socketserver import ThreadingMixIn
import json
from urllib.parse import urlparse, parse_qs
from types import SimpleNamespace

from config import *

SimpleHTTPRequestHandler.extensions_map = {k: v + ';charset=UTF-8' for k, v in SimpleHTTPRequestHandler.extensions_map.items()}

class ThreadingServer(ThreadingMixIn, HTTPServer):
    pass


class BasicServer(BaseHTTPRequestHandler):
    
    @property
    def _url(self):
        return urlparse((self.path))

    @property
    def _query(self):
        return SimpleNamespace(**parse_qs((self._url.query)))


    def send_content(self, res, content, content_type=DEFAULT_SERVER_CONTENT_TYPE):
        self.send_response(res)
        self.send_header("Content-Type", content_type)
        self.end_headers()
        self.wfile.write(content)


    def _read_data(self):
        size = int(self.headers.get("Content-Length", 0))
        return self.rfile.read(size)

    @property
    def _json_data(self):
        data = self._read_data()
        return json.loads(data)


    def default_route_request(self, method):
        path = self._url.path[1:].replace("/", "_")
        handler_name = f"handle_{method}_{path}"
        
        if hasattr(self, handler_name):
            handler = getattr(self, handler_name)
            return handler()

        self.send_content(404, b"Page Not Found")
        return None


    @classmethod
    def get_threading_Server(cls, addr):
        return ThreadingServer(addr, cls)


# Super Simple Audio Server
a simple way to play audio files on your pc

playing an mp3 file on multiple devices - the device parameters can be substrings of the actual device name
```bash
curl http://127.0.0.1:8080/play\?path\="/path/to/audio.mp3"\&device\="mixaudio"\&device\="hyper"
```

you also have `create_loopback.sh` that creates a mic loopback device which turns this server (+ some curl keyboard shortcuts)
into a soundboard